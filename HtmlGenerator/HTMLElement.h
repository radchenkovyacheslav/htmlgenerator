#pragma once
#include <string>

using std::string;

class HTMLElement
{
protected:
	string textContent;
public:
	HTMLElement();
	HTMLElement(string content);
	void setTextContent(string content);
	string getTextContent();
	virtual string render();
	virtual ~HTMLElement();
};

