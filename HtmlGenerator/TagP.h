#pragma once
#include "HTMLElement.h"
class TagP :
	public HTMLElement
{
public:
	TagP();
	virtual string render();
	TagP(string content);
	~TagP();
};

