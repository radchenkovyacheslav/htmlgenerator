#include "TagP.h"
#include "HTMLElement.h"
#include <iostream>
using std::cout;
using std::endl;
void main() {
	TagP p;
	p.setTextContent("hello world");
	cout << p.render() << endl;

	TagP arr[5] = { "sadfasf", "asdfasdf","adfafsd",
		"sdfasdf","asdfdasf" };

	for (int i = 0; i < 5; i++) {
		cout << arr[i].render() << endl;
	}

	HTMLElement* arr2[3] = { new TagP("p1"), new TagP("p2"), new TagP("p3") };

	for (int i = 0; i < 3; i++) {
		cout << arr2[i]->render() << endl;
		delete arr2[i];
	}

}